/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.consoleio;

/**
 *
 * @author student
 */
public class Professor {

    public String фамилия;
    private String имя;
    private String отчество;

    public String getФамилия() {
        return фамилия;
    }

    public void setФамилия(String фамилия) {
        this.фамилия = фамилия;
    }

    public String getИмя() {
        return имя;
    }

    public void setИмя(String имя) {
        this.имя = имя;
    }

    public String getОтчество() {
        return отчество;
    }

    public void setОтчество(String отчество) {
        this.отчество = отчество;
    }

    @Override
    public String toString() {
        return "Professor{" + "\u0444\u0430\u043c\u0438\u043b\u0438\u044f=" + фамилия + ", \u0438\u043c\u044f=" + имя + ", \u043e\u0442\u0447\u0435\u0441\u0442\u0432\u043e=" + отчество + '}';
    }
    
}

