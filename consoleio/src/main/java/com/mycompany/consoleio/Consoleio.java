/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.consoleio;

/**
 *
 * @author student
 */
public class Consoleio {

    public static void main(String[] args) {
        System.out.println("Создание экземпляра класса студент...");
        Student st1 = new Student();
        st1.setФамилия("Королёв");
        st1.setИмя("Андрей");
        st1.setОтчество("Иванович");
        st1.setКурс("1");
        st1.setГруппа("ПИ-Б");
        System.out.println(st1.toString());

        System.out.println("Создание экземпляра класса профессор...");
        Professor p1 = new Professor();
        p1.setФамилия("Путин");
        p1.setИмя("Владимир");
        p1.setОтчество("Владимирович");
        System.out.println(p1.toString());

        System.out.println("Создание экземпляра класса персон...");
        Person p2 = new Person();
        p2.setФамилия("Подшивалов");
        p2.setИмя("Алексей");
        p2.setОтчество("Алексеевич");
        System.out.println(p2.toString());
    }

}
